package OOP_java1;

public interface TruckEngine {
    public String fuel();
    public Integer capacity();
    public Integer euroType();

}
