package OOP_java1;

public class Ford {

    public Ford(int wheels, int doors, int gearbox, String color) {
        this.wheels = wheels;
        this.doors = doors;
        this.gearbox = gearbox;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Ford{" +
                "wheels=" + wheels +
                ", doors=" + doors +
                ", gearbox=" + gearbox +
                ", color='" + color + '\'' +
                '}';
    }

    private int wheels;
    private int doors;
    private int gearbox;
    private String color;

    public Ford(){
       wheels = 4;
       doors = 4;
       gearbox = 6;
       color="";

    }


    public int getWheels() {
        return wheels;
    }

    public int getDoors() {
        return doors;
    }

    public int getGearbox() {
        return gearbox;
    }

    public String pressHorn(){

        return " Tuut.. Tut..Tut ";
    }

}
