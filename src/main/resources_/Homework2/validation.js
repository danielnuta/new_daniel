function validate() {
  var ssn = document.getElementById("ssn").value;
  var day = document.getElementById("day").value;
  var month = document.getElementById("month").value;
  var year = document.getElementById("year").value;


  var yearFromSSN = ssn.substring(1, 3);
  var monthFromSSN = ssn.substring(3, 5);
  var dayFromSSN = ssn.substring(5, 7);

  var gender = ssn.substring(0, 1);

  var errors = "";

  if (ssn.length !== 13) {
    errors = errors + "SSN must have 13 characters <br>";
  }

  if (day === "") {
    errors = errors + "Day must not be empty <br>";
  } else {
    if (isNaN(day)) {
      errors = errors + "Day must be a number <br>";
    } else {
      if (day < 1) {
        errors = errors + "Day must be greater than or equal to 1 <br>";
      }

      var maxDay = 31;

      if (month === "4" || month === "6" || month === "9" || month === "11") {
        maxDay = 30;
      }

      if (month === "2") {
        maxDay = 28;

        if (leapYear(year)) {
          maxDay = 29;
        }
      }

      if (day > maxDay) {
        errors =
          errors + "Day must be less than or equal to " + maxDay + " <br>";
      }
    }
  }


  if (month === "") {
    errors = errors + "Month must not be empty <br>";
  }
  if (year === "") {
    errors = errors + "Year must not be empty <br>";
  }

  document.getElementById("validation-result").innerHTML = errors;
}

