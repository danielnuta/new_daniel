package main.java.resources_new.H07_Inheritance;

public class Person {
        private String firstName;
        private String lastName;
        private String address;
        private String city;
        private String country;
        private String birthDate;

        public Person(String firstName, String lastName, String address, String city, String country, String birthDate, Double salary) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.city = city;
            this.country = country;
            this.birthDate = birthDate;
        }

        public Person() {
        }

        @Override
        public String toString() {
            return "Person:" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", birthDate='" + birthDate + '\'';
        }


}
