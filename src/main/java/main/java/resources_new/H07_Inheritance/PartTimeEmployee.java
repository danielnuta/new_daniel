package main.java.resources_new.H07_Inheritance;

public class PartTimeEmployee extends Employee {


     Double salary;

     public PartTimeEmployee(){
         super.setSalary(Double.valueOf(3000));
         this.salary = getSalary();
     }


    public PartTimeEmployee (String firstName, String lastName, String address, String city, String country, String birthDate, Double salary) {
        super(firstName, lastName, address, city, country, birthDate, salary);
        setSalary(salary - salary * 0.25);
    }
}
