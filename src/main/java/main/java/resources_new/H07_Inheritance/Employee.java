package main.java.resources_new.H07_Inheritance;

public class Employee extends Person {
    private Double salary;

    public Employee(){
        this.salary = Double.valueOf(3000);
    }

    public Employee (String firstName, String lastName, String address, String city, String country, String birthDate, Double salary) {
        super(firstName, lastName, address, city, country, birthDate);
        this.salary = salary;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() +
                "salary=" + salary;


    }
}
