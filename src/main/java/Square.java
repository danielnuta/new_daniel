//package main.java;
//
//public class Square {
//
//    //vizibil doar in clasa
//     private double side; //default is 0
//
//
//     //vizibil in pachet si in subclase
//   protected void setSide(double side) {
//       if (side < 0 ) {
//           System.out.println("Latura trebuie mai mare  ca 0");
//       }
//       else {
//        this.side = side;
//    }
//
//    //vizibil oriunde
//    public double getArea() {
//        return side * side;
//    }
//
//    public Square (){
//
//    }
//
//    public  Square (double side){
//        this.side = side;
//
//    }
//}
